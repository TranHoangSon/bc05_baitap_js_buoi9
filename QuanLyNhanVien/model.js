function staffModel(
  accountStaff,
  nameStaff,
  emailStaff,
  passStaff,
  salaryStaff,
  dayStaff,
  timeStaff,
  text
) {
  this.account = accountStaff;
  this.name = nameStaff;
  this.email = emailStaff;
  this.pass = passStaff;
  this.salary = salaryStaff * 1;
  this.day = dayStaff;
  this.time = timeStaff;
  this.position = text;
  this.totalSalary = function () {
    var total = 0;
    if (text == "Sếp") {
      total = salaryStaff * 3;
    } else if (text == "Trưởng phòng") {
      total = salaryStaff * 2;
    } else {
      total = salaryStaff;
    }
    return total;
  };

  this.classify = function () {
    var classifyStaff = "";
    if (timeStaff < 160) {
      classifyStaff = "Nhân viên trung bình";
    } else if (timeStaff < 176) {
      classifyStaff = "Nhân viên khá";
    } else if (timeStaff < 192) {
      classifyStaff = "Nhân viên giỏi";
    } else {
      classifyStaff = "Nhân viên xuất sắc";
    }
    return classifyStaff;
  };
}
