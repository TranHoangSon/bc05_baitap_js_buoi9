// Không được để trống
function blankTest(accountInput, idErr, message) {
  if (accountInput.length == 0) {
    showMessageErr(idErr, message);
    return false;
  } else {
    showMessageErr(idErr, "");
    return true;
  }
}
// Tài khoảng tối đa 6 ký số
function validateNumber(value, idErr, message) {
  var reg = /^[a-zA-Z0-9]*$/;

  let isNumber = reg.test(value);
  if (value.length < 6) {
    if (isNumber) {
      showMessageErr(idErr, "");
      return true;
    }
  } else {
    showMessageErr(idErr, message);
    return false;
  }
}
// Không trùng id
function duplicatesAccount(account, listStaff) {
  let local = searchAccount(account, listStaff);
  if (local !== -1) {
    showMessageErr("tbTKNV", "Account đã tồn tại");
    return false;
  } else {
    showMessageErr("tbTKNV", "");
    return true;
  }
}
// Tên đăng nhập là chữ
function validateName(value, idErr, message) {
  var reg = /^[a-z A-Z0]*$/;
  let isText = reg.test(value);
  if (isText) {
    showMessageErr(idErr, "");
    return true;
  } else {
    showMessageErr(idErr, message);
    return false;
  }
}
// Validate Email
function validateEmail(value, idErr, message) {
  const reg =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  let isEmail = reg.test(value);
  if (isEmail) {
    showMessageErr(idErr, "");
    return true;
  } else {
    showMessageErr(idErr, message);
    return false;
  }
}
// validate password
function validatePassword(value, idErr, message) {
  const reg =
    /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$@!%&*?])[A-Za-z\d#$@!%&*?]{6,10}$/;
  let isPass = reg.test(value);
  if (isPass) {
    showMessageErr(idErr, "");
    return true;
  } else {
    showMessageErr(idErr, message);
    return false;
  }
}
