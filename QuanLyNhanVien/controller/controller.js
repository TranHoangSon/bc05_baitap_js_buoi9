function getValue() {
  var accountStaff = document.getElementById("tknv").value;
  var nameStaff = document.getElementById("name").value;
  var emailStaff = document.getElementById("email").value;
  var passStaff = document.getElementById("password").value;
  var salaryStaff = document.getElementById("luongCB").value;
  var dayStaff = document.getElementById("datepicker").value;
  var timeStaff = document.getElementById("gioLam").value;
  var e = document.getElementById("chucvu");
  var text = e.options[e.selectedIndex].text;
  var staff = new staffModel(
    accountStaff,
    nameStaff,
    emailStaff,
    passStaff,
    salaryStaff,
    dayStaff,
    timeStaff,
    text
  );
  return staff;
}

function renderStaff(staffArr) {
  var contentHtml = "";
  for (var i = 0; i < staffArr.length; i++) {
    var currentStaff = staffArr[i];

    var contenTr = `
        <tr>
        <td>${currentStaff.account}</td>
        <td>${currentStaff.name}</td>
        <td>${currentStaff.email}</td>
        <td>${currentStaff.day}</td>
        <td>${currentStaff.position}</td>
        <td>${currentStaff.totalSalary()}</td>
        <td>${currentStaff.classify()}</td>
        <td><button class="btn btn-danger" onclick="deleteStaff('${
          currentStaff.account
        }')">Xóa</button></td>
        <td>
        <button class="btn btn-success" data-target="#myModal" onclick="updateStaff('${
          currentStaff.account
        }')" id="update">Cập nhật</button></td>
    
        </tr>
        `;
    contentHtml += contenTr;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHtml;
}
function searchAccount(account, listStaff) {
  for (var i = 0; i < listStaff.length; i++) {
    var item = listStaff[i];
    if (item.account == account) {
      return i;
    }
  }
  return -1;
}
function showMessageErr(idErr, message) {
  document.getElementById(idErr).innerText = message;
  document.getElementById(idErr).style.display = "block";
}
function resetForm() {
  document.getElementById("FormQLNV").reset();
}

function showInfor(staff) {
  document.getElementById("tknv").value = staff.account;
  document.getElementById("name").value = staff.name;
  document.getElementById("email").value = staff.email;
  document.getElementById("password").value = staff.pass;
  document.getElementById("luongCB").value = staff.salary * 1;
  document.getElementById("datepicker").value = staff.day;
  document.getElementById("gioLam").value = staff.time * 1;
  var e = document.getElementById("chucvu");
  e.options[e.selectedIndex].text = staff.position;
}
function search(arr) {
  document.getElementById("btnTimNV").addEventListener("click", function () {
    var data = arr;
    var searchName = document.getElementById("searchName").value;

    var arrList = [];
    for (let i = 0; i < data.length; i++) {
      var str = data[i].name;

      if (str.includes(searchName)) {
        arrList.push(data[i]);
      } else {
        console.log("no");
      }
      renderStaff(arrList);
    }
  });
}
