var listStaff = [];
var dataJson = localStorage.getItem("ListStaff");

if (dataJson !== null) {
  var staffArr = JSON.parse(dataJson);
  for (var i = 0; i < staffArr.length; i++) {
    var item = staffArr[i];
    var staffItem = new staffModel(
      item.account,
      item.name,
      item.email,
      item.pass,
      item.salary,
      item.day,
      item.time,
      item.position
    );
    listStaff.push(staffItem);
  }
  renderStaff(listStaff);
}
// search
search(listStaff);

function saveLocalStorage() {
  var jsonStaff = JSON.stringify(listStaff);
  localStorage.setItem("ListStaff", jsonStaff);
}
function themNV() {
  document.getElementById("tknv").disabled = false;
  var btn = document.getElementById("btnThemNV");
  btn.style.display = "block";
  var remove = document.getElementById("btnCapNhat");
  remove.style.display = "none";
  var message = document.querySelectorAll(".sp-thongbao");
  // for (let i = 0; i < message.length; i++) {
  //   var item = message[i];
  //   item.style.display = "none";
  // }
  message.forEach((item) => {
    item.style.display = "none";
  });
  resetForm();
}
function addStaff() {
  var staff = getValue();

  var isValid = true;
  // validate Account
  isValid =
    blankTest(staff.account, "tbTKNV", "Không được để trống") &&
    validateNumber(staff.account, "tbTKNV", "nhỏ hơn 6 ký số") &&
    duplicatesAccount(staff.account, listStaff);
  // validate Name
  isValid =
    isValid &
    (blankTest(staff.name, "tbTen", "Không được để trống") &&
      validateName(staff.name, "tbTen", "Tên phải là chữ"));
  // validate Email
  isValid =
    isValid &
    (blankTest(staff.email, "tbEmail", "Không được để trống") &&
      validateEmail(staff.email, "tbEmail", "Email không hợp lệ"));
  isValid =
    isValid &
    (blankTest(staff.pass, "tbMatKhau", "Password không được để trống") &&
      validatePassword(staff.pass, "tbMatKhau", "Password không hợp lệ"));
  if (isValid) {
    listStaff.push(staff);
    saveLocalStorage();

    renderStaff(listStaff);
  }
}
function deleteStaff(account) {
  var local = searchAccount(account, listStaff);
  console.log("local: ", local);
  if (local !== -1) {
    listStaff.splice(local, 1);
    renderStaff(listStaff);
  }
  saveLocalStorage();
}
function update() {
  var data = getValue();

  var local = searchAccount(data.account, listStaff);
  if (local == -1) return;
  listStaff[local] = data;
  renderStaff(listStaff);
  saveLocalStorage();
}
function updateStaff(account) {
  // ngăn cản edit account
  document.getElementById("tknv").disabled = true;
  var index = document.getElementById("btnThem");
  index.click();
  var local = searchAccount(account, listStaff);
  if (local == -1) return;
  var data = listStaff[local];
  showInfor(data);
  var btn = document.getElementById("btnThemNV");
  btn.style.display = "none";

  var remove = document.getElementById("btnCapNhat");
  remove.style.display = "block";
  // seach

  // index.style.display = "block";
  // index.setAttribute("class", "modal fade show");
  // var btn = document.getElementById("btnThemNV");
  // btn.style.display = "none";
}

// console.log(/^([a-z0-9]{6,10})$/.test("abc12a122111"));
